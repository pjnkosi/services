/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.demo.repository;

import app.demo.entity.Tasks;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author PJ NKOSI
 */
public interface TasksRepository extends JpaRepository<Tasks, Long>{
    
}
