/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.demo.controllers;

import app.demo.entity.Tasks;
import app.demo.repository.TasksRepository;
import java.util.List;
import javafx.concurrent.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PJ NKOSI
 */
@RestController
@RequestMapping("/api")
public class TasksController {
    
    private final TasksRepository tasksRepository;
    
    @Autowired
    public TasksController(TasksRepository tasksRepository) {
        this.tasksRepository = tasksRepository;
    }
    
    
    @GetMapping("/tasks")
    public List<Tasks> listTasks(){
        return tasksRepository.findAll();
    }
    
    @PostMapping("/create")
    public String createTask(@RequestBody Tasks task){
          task.setStatus("In Complete");
          tasksRepository.save(task);
        return "phumlani";
    }
    
     @RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
	public void deleteTask(@PathVariable long id) {
		tasksRepository.deleteById(id);
	}
        
    @RequestMapping(value = "update/{id}", method = RequestMethod.PUT)
     public void update(@PathVariable long id, @RequestBody Tasks newTask){
        tasksRepository.findById(id).map(task->{
            task.setTitle(newTask.getTitle());
            task.setDueDate(newTask.getDueDate());
            task.setCategory(newTask.getCategory());
            task.setDescription(newTask.getDescription());
            task.setStatus(newTask.getStatus());
           return tasksRepository.save(task);
        });
     }
     
     @RequestMapping(value = "change/{id}", method = RequestMethod.PUT)
     public void changeStatus(@PathVariable long id, @RequestBody Tasks newTask){
        tasksRepository.findById(id).map(task->{
            task.setStatus(newTask.getStatus());
           return tasksRepository.save(task);
        });
     }
        
    
        

    
}
